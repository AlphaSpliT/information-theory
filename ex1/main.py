import json
import sys
from itertools import product

import numpy as np


def prodXY():
    return product([0, 1], [0, 1])


def read_from_file(filename):
    with open(filename, 'r') as f:
        lines = f.read().split('\n')
        return list(map(lambda x: float(x), lines))


def H(X):
    return np.sum(np.log2(1 / X) * X)


def CALCULATE_I(prob_dict):
    return np.sum([prob_dict[f'x{x},y{y}'] * np.log2(prob_dict[f'x{x},y{y}'] / (prob_dict[f'x{x}'] * prob_dict[f'y{y}'])) for (x, y) in prodXY()])


def my_print(text, number):
    print(f'{text} = {number:.6f}')


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <inputfile>')
        exit(1)

    px1, py1x0, py1x1 = read_from_file(sys.argv[1])
    px0 = 1 - px1

    py0x0 = 1 - py1x0
    py0x1 = 1 - py1x1

    py1 = (px0 * py1x0) + (px1 * py1x1)
    py0 = 1 - py1

    px0y0 = (py0x0 * px0) / py0
    px1y0 = (py0x1 * px1) / py0
    px0y1 = (py1x0 * px0) / py1
    px1y1 = (py1x1 * px1) / py1

    p = {
        'x0': px0,
        'x1': px1,
        'y1': py1,
        'y0': py0,
        'x0y0': px0y0,
        'x0y1': px0y1,
        'x1y0': px1y0,
        'x1y1': px1y1,
        'y0x0': py0x0,
        'y0x1': py0x1,
        'y1x0': py1x0,
        'y1x1': py1x1,
        'x0,y0': px0y0 * py0,
        'x0,y1': px0y1 * py1,
        'x1,y0': px1y0 * py0,
        'x1,y1': px1y1 * py1,
        'y0,x0': py0x0 * px0,
        'y0,x1': py0x1 * px1,
        'y1,x0': py1x0 * px0,
        'y1,x1': py1x1 * px1
    }

    X = np.array([px0, px1])
    Y = np.array([py0, py1])
    Z = np.array([p[f'x{x},y{y}'] for (x, y) in prodXY()])
    Z_ = np.array([p[f'y{y},x{x}'] for (x, y) in prodXY()])

    my_print('H(X)', H(X))
    my_print('H(Y)', H(Y))
    my_print('H(X,Y)', H(Z))
    my_print('H(X|Y)', H(Z) - H(Y))
    my_print('H(Y|X)', H(Z_) - H(X))
    my_print('I(X;Y)', CALCULATE_I(p))
    my_print('D(P(X)||P(Y))', p["x0"] * np.log2(p["x0"] /
                                                p["y0"]) + p["x1"] * np.log2(p["x1"] / p["y1"]))
    my_print('D(P(Y)||P(X))', p["y0"] * np.log2(p["y0"] /
                                                p["x0"]) + p["y1"] * np.log2(p["y1"] / p["x1"]))

    print(H(np.array([0.17, 0.15, 0.39, 0.04, 0.09, 0.16])))