# Exercise 1

Adott X, Y bináris (Bernoulli) valószínűségi változók, melyek között az X -> Y kapcsolat áll fenn, és ismerjük a P(X)-et, illetve a P(Y|X)-eket.
Írjatok programot (bármilyen programozási nyelvben), amely kiszámítja a következőket:
- H(X), H(Y)
- H(X,Y), H(X|Y), H(Y|X)
- I(X;Y)
- D(P(X)||P(Y))
- D(P(Y)||P(X))

A program a bemeneteket egy háromsoros szöveges fájlból kapja, ahol a sorokban szereplő értékek a következőket jelentik:
- P(X=1)
- P(Y=1|X=0)
- P(Y=1|X=1)

A kimenetet a standard kimeneten jelenítsük meg.
Futtassuk a programot a következő bemenetekre:

```
0.2
0.1
0.7
```
## A helyes kimenet:

```
H(X) = 0.721928
H(Y) = 0.760168
H(X,Y) = 1.273383
H(X|Y) = 0.513215
H(Y|X) = 0.519755
I(X;Y) = 0.208713
D(P(X)||P(Y)) = 0.001720
D(P(Y)||P(X)) = 0.001761
```