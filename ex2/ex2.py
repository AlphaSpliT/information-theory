import numpy as np
import random
import string

def H(X):
    return np.sum(np.log2(1 / X) * X)

class Node:
    def __init__(self, value=None, left=None, right=None, uid=None):
        self.value = value
        self.left = left
        self.right = right
        self.id = uid
    
    def __str__(self):
        return f'<node value="{self.value}" id={self.id} />'

def depth_of_char(root, uid, d=0):
    if type(root.value) != list:
        if root.id == uid:
            return d
    
    dr = depth_of_char(root.right, uid, d=d+1) if root.right else None
    dl = depth_of_char(root.left, uid, d=d+1) if root.left else None
    
    if dr and dl:
        return dr if dr < dl else dl
    
    return dr if dr else dl

def walk_from_root(root, depth=0):
    if root:
        walk_from_root(root.left, depth=depth+1)
        walk_from_root(root.right, depth=depth+1)
        print(' ' * depth, str(root))

def sel_n_min(array, n=2):
    sorted_array = sorted(array, key=lambda node: node.value)
    return sorted_array[:2], sorted_array[2:]

def build_huffman_tree(array):
    nodes = list(map(lambda obj: Node(value=obj[0], uid=obj[1]), array))
    while len(nodes) != 1:
        [node1, node2], nodes = sel_n_min(nodes)
        new_node = Node(value=node1.value + node2.value, left=node1, right=node2)
        nodes = [new_node, *nodes]

    return nodes[0]

def build_shannon_tree(array):
    def calculate_sum_diff(array1, array2):
        return np.abs(sum(list(map(lambda obj: obj[0], array1))) - sum(list(map(lambda obj: obj[0], array2))))
    
    def generate_all_possible_divisions(an_array):
        return [(an_array[:i], an_array[i:]) for i in range(0, len(an_array), 1)]

    def divide_in_two(an_array):
        all_possible_divisions = generate_all_possible_divisions(an_array)
        return min(all_possible_divisions, key=lambda tupl: calculate_sum_diff(tupl[0], tupl[1]))
    
    root = Node()
    if len(array) > 1:
        s_array = sorted(array, reverse=True)
        root.value = s_array
        left, right = divide_in_two(s_array)
        root.left = build_shannon_tree(left)
        root.right = build_shannon_tree(right)
    else:
        root.value = array[0][0]
        root.id = array[0][1]
    
    return root


def calculate_codelength(root, sorted_obj):
    return np.sum(list(map(lambda obj: depth_of_char(root, obj[1]) * obj[0], sorted_obj)))

inpt = np.sort(np.array([0.17, 0.15, 0.39, 0.04, 0.09, 0.16]))
letters = random.sample(string.ascii_letters, len(inpt))
sorted_inpt = sorted(inpt)
sorted_obj = [(sorted_inpt[i], letters[i]) for i in range(len(sorted_inpt))]
root = build_huffman_tree(sorted_obj)
root1 = build_shannon_tree(sorted_obj)

print('------------------------')
print(H(inpt))
print(calculate_codelength(root, sorted_obj))
print(f'{calculate_codelength(root1, sorted_obj)}')
print('------------------------')