# Exercise 2 Huffman- és Shannon-Fano kódok.
Adottak a kódolandó szimbólumok megjelenési valószínűségei (magukat a szimbólumokat nem szükséges ismernünk jelen esetben). Implementáljuk a (bináris és statikus) Huffman- és Shannon-Fano kódolást, határozzuk meg a kódszavakat, illetve azok hosszait, majd számoljuk ki a következőket (tetszőleges programozási nyelvben):

  - A val. változó entrópiája
  - Huffman-kód átlagos kódszóhossza
  - Shannon-Fano-kód átlagos kódszóhossza

A program a bemeneteket egy n sort tartalmazó szöveges fájlból kapja, ahol minden sorban egy szimbólum
megjelenési valószínűsége található. A kimenetet a standard kimeneten jelenítsük meg.

###  Futtassuk a programot a következő bemenetre:

```
[0.17, 0.15, 0.39, 0.04, 0.09, 0.16]
```

## Elvárt kimenet:
```
2.2963538019348038
2.35
2.41
```