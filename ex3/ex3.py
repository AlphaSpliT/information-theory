import sys

from node import Node


def build_code_dict(root, curr_code='', dct={}):
    for label in root.children.keys():
        build_code_dict(root.children[label], curr_code=curr_code + label)

    if len(root.children.keys()) == 0:
        dct[root.id] = curr_code

    return dct


def sel_n_min(array, n=2):
    indexed = sorted(list(enumerate(array)), key=lambda tupl: tupl[1].value)
    first_n = indexed[:n]
    sorted_by_index = [node for (index, node) in sorted(
        first_n, key=lambda tupl: tupl[0])]
    remaining = list(filter(lambda node: node not in sorted_by_index, array))
    remaining = [None if node in sorted_by_index else node for node in array]
    return sorted_by_index, remaining


def build_huffman_tree(array, language):
    s = len(language)
    n = len(array)
    k = (n - 2) % (s - 1) + 2
    first_step = True

    nodes = list(map(lambda obj: Node(value=obj[0], uid=obj[1]), array))

    while len(nodes) != 1:
        if first_step:
            currently_combined = k
            needed, remaining = sel_n_min(nodes, k)
            first_step = False
        else:
            currently_combined = s
            needed, remaining = sel_n_min(nodes, s)

        value = sum([node.value for node in needed])
        children = dict([(language[i], needed[i]) for i in range(len(needed))])
        new_node = Node(value=value, children=children)

        start = remaining.index(None)
        nodes = [*nodes[:start], new_node, *nodes[start + currently_combined:]]
    return nodes[0]


if len(sys.argv) < 2:
    print(f'Usage: {sys.argv[0]} <input-file>')
    exit(1)


with open(sys.argv[1], 'r') as f:
    X, Y, ps, message = list(
        map(lambda line: line.strip().split(' '), f.read().split('\n')))
    Y = sorted(Y)
    ps = [float(p) for p in ps]
    sorted_obj = [(ps[i], X[i]) for i in range(len(X))]
    root = build_huffman_tree(sorted_obj, Y)

    code_dict = build_code_dict(root)
    print(code_dict)
    print(''.join([code_dict[c] for c in message]))
