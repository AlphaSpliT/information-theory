class Node:
    def __init__(self, value=None, uid=None, children={}):
        self.value = value
        self.children = children
        self.id = uid

    def __str__(self):
        return f'<node value="{self.value}" id={self.id} />'
