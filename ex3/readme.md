# s-áris (s-edfokú) Huffman-kód

# Usage
Example:

```powershell
python .\ex3.py .\input
{'a': '000', 'b': '001', 'c': '01', 'd': '02', 'e': '1', 'f': '2'}
001122000
```

# Feladat

## Írjunk programot s-áris Huffman-kód generálására.

### Bemenet
X (azaz a forrásábécé), Y (azaz a kódábécé elemei; az Y méretéből meglesz az s), p (azaz a szimbólumok megjelenési valószínűségei), u (az üzenet, vagyis a kódolandó szimbólumsorozat).

### Kimenet
Az s-áris Huffman-kód.

### Konvenció
A kódábécé szimbólumait rendezzük lexikografikus sorrendbe (azaz ábécésorrendbe), és balról jobbra mindig ebben a sorrendben rendeljük ezeket egy csomópont kimenő éleihez.

## _Megjegyzés_
Csak a kódolást kell megírni, a dekódolást nem!

### Bemenet
A program a bemenetet egy szöveges állományból kapja:
1. sor: az X elemei szóközökkel elválasztva
2. sor: az Y elemei szóközökkel elválasztva
3. sor: p valószínűségei szóközökkel elválasztva (X és p sorrendje meg kell egyezzen)
4. sor: az üzenet szóközökkel elválasztva

## Pelda
**Bemenet**:
```
a b c d e f
0 1 2
0.1 0.1 0.15 0.15 0.25 0.25
b e f f a
```
**Kimenet**
```
001122000
```