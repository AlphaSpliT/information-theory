import sys
import numpy as np
from node import Node
import random
import string


def H(X):
    return np.sum(np.log2(1 / X) * X)


def depth_of_char(root, uid, d=0):
    if type(root.value) != list:
        if root.id == uid:
            return d

    dr = depth_of_char(root.right, uid, d=d+1) if root.right else None
    dl = depth_of_char(root.left, uid, d=d+1) if root.left else None

    if dr and dl:
        return dr if dr < dl else dl

    return dr if dr else dl


def walk_from_root(root, depth=0):
    if root:
        walk_from_root(root.left, depth=depth+1)
        walk_from_root(root.right, depth=depth+1)
        print(' ' * depth, str(root))


def sel_n_min(array, n=2):
    sorted_array = sorted(array, key=lambda node: node.value)
    return sorted_array[:2], sorted_array[2:]


def build_huffman_tree(array):
    nodes = list(map(lambda obj: Node(value=obj[0], uid=obj[1]), array))
    while len(nodes) != 1:
        [node1, node2], nodes = sel_n_min(nodes)
        new_node = Node(value=node1.value + node2.value,
                        left=node1, right=node2)
        nodes = [new_node, *nodes]

    return nodes[0]


def build_shannon_tree(array):
    def calculate_sum_diff(array1, array2):
        return np.abs(sum(list(map(lambda obj: obj[0], array1))) - sum(list(map(lambda obj: obj[0], array2))))

    def generate_all_possible_divisions(an_array):
        return [(an_array[:i], an_array[i:]) for i in range(0, len(an_array), 1)]

    def divide_in_two(an_array):
        all_possible_divisions = generate_all_possible_divisions(an_array)
        return min(all_possible_divisions, key=lambda tupl: calculate_sum_diff(tupl[0], tupl[1]))

    root = Node()
    if len(array) > 1:
        s_array = sorted(array, reverse=True)
        root.value = s_array
        left, right = divide_in_two(s_array)
        root.left = build_shannon_tree(left)
        root.right = build_shannon_tree(right)
    else:
        root.value = array[0][0]
        root.id = array[0][1]

    return root


def calculate_codelength(root, sorted_obj):
    return np.sum(list(map(lambda obj: depth_of_char(root, obj[1]) * obj[0], sorted_obj)))


def generate_probability_array(n):
    assert n >= 2, 'Having one probabillity makes no sense'
    array = [random.uniform(0, 0.6)]
    for _ in range(1, n-1):
        array.append(random.uniform(0, 1 - sum(array)))

    _sum = sum(array)

    array.append(1 - _sum)
    letters = random.sample(string.ascii_lowercase, n)

    return [(array[i], letters[i]) for i in range(n)]


if len(sys.argv) == 1:
    print(
        f'Usage: {sys.argv[0]} <number-of-source-symbols> <number-of-source-symbols> <number-of-source-symbols> ... <number-of-source-symbols>')
    sys.exit(1)
else:
    try:
        for numb in map(int, sys.argv[1:]):
            pass
    except ValueError as e:
        print('Only numbers should be provided as input parameters')
        sys.exit(1)

for i in map(int, sys.argv[1:]):
    print(f'Running for {i} source symbols:')

    failure = 0

    for _ in range(10 ** 4):
        prob = sorted(generate_probability_array(i), key=lambda tupl: tupl[0])
        root = build_huffman_tree(prob)
        shannon_root = build_shannon_tree(prob)
        failure += 0 if round(calculate_codelength(root, prob),
                              6) == round(calculate_codelength(shannon_root, prob), 6) else 1

    print(f'Success rate: {((10 ** 4 - failure) / 10 ** 2)}%')
