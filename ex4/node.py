class Node:
    def __init__(self, value=None, left=None, right=None, uid=None):
        self.value = value
        self.left = left
        self.right = right
        self.id = uid

    def __str__(self):
        return f'<node value="{self.value}" id={self.id} />'