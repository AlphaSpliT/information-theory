# Shanno-Fano vs Huffman

Bizonyítsuk be program segítségével, hogy 3, illetve 4 forrásszimbólum (és bináris kódábécé) esetén a Huffman és a Shannon-Fano kódok mindig megegyeznek (vagyis az átlagos kódszóhosszok egyenlőek).
Végezzünk legalább 10^4 tesztet, azaz legyen P(E|f_{H}(X)| != E|f_{SF}(X)|) < 10^-4.
Az átlagos kódszóhosszokat 6 tizedes pontossággal határozzuk meg (összehasonlításkor).

## Usage
```bash
python ex4.py <number-of-source-symbols> <number-of-source-symbols> <number-of-source-symbols> ... <number-of-source-symbols>
```

## Example
```bash
python ex4.py 3 4 10
Running for 3 source symbols:
Success rate: 100.0%
Running for 4 source symbols:
Success rate: 100.0%
Running for 10 source symbols:
Success rate: 95.61%
```