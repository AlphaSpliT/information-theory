import sys
import re
from string import digits


class Node:
    def __init__(self, value=None, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right


def split_input(inp):
    split_by_space = re.split(r' ', inp)
    buffer = ''
    splt = []
    for char in inp.replace(' ', ''):
        if char in digits:
            buffer += char
        else:
            if buffer:
                splt.append(buffer)
            splt.append(char)
            buffer = ''
    return splt


def get_value(inp):
    return int(inp[1] if inp[0] == '(' else inp[0])


def get_left_ending_index(inp):
    depth = 1
    index = 3 if inp[0] == '(' else 2
    while depth != 0:
        if inp[index] == '(':
            depth += 1
        if inp[index] == ')':
            depth -= 1
        index += 1
    return index


def get_left(inp):
    if len(inp) == 1 or ')' not in inp:
        return None

    index = get_left_ending_index(inp) - 1
    start_index = 3 if inp[0] == '(' else 2
    return inp[start_index:index]


def get_right_ending_index(inp):
    index = get_left_ending_index(inp) + 2
    depth = 1
    while depth != 0:
        if inp[index] == '(':
            depth += 1
        if inp[index] == ')':
            depth -= 1
        index += 1
    return index


def get_right(inp):
    if len(inp) == 1 or ')' not in inp:
        return None

    start_index = index = get_left_ending_index(inp) + 1
    end_index = get_right_ending_index(inp) - 1
    return inp[start_index:end_index]


def build_tree(inp):
    root = Node()
    root.value = get_value(inp)
    left = get_left(inp)
    right = get_right(inp)

    if left:
        root.left = build_tree(left)

    if right:
        root.right = build_tree(right)

    return root


def walk_tree(root):
    def _walk_tree(root):
        string = f' ({root.value}'

        if root.left:
            string += f' {_walk_tree(root.left)}'

        if root.right:
            string += f' {_walk_tree(root.right)}'

        string += ')'

        return string.strip()

    return _walk_tree(root)[1:-1]


def brotherhoodpair(root, ls=[]):
    def all_nodes_have_brothers(root):
        if (root.left == None and root.right != None) or (root.right == None and root.left != None):
            return False

        value = True

        if root.left:
            value = value and all_nodes_have_brothers(root.left)

        if root.right:
            value = value and all_nodes_have_brothers(root.right)

        return value

    def build_pair_array(root, array=[]):
        if root.left:
            bigger = root.left if root.left.value > root.right.value else root.right
            smaller = root.left if bigger == root.right else root.right
            array.append(bigger.value)
            array.append(smaller.value)
            build_pair_array(bigger, array=array)
            build_pair_array(smaller, array=array)

        return array
    
    pair_array = build_pair_array(root)
    return all_nodes_have_brothers(root) and pair_array == sorted(pair_array, reverse=True)

if len(sys.argv) == 1 or len(sys.argv) > 2:
    print(f'Usage: {sys.argv[0]} <input-file>')
    exit(1)

with open(sys.argv[1], 'r') as f:
    lines = f.read().split('\n')

    for line in lines:
        try:
            recognized_format = split_input(line)
            root = build_tree(recognized_format)
            assert line == walk_tree(root), f'Something went wrong while reading the file for line {line[:100]} {("..." if len(line) > 100 else "")}'
            print('Bemenet:', walk_tree(root))
            print('Kimenet:', 'Teljesül' if brotherhoodpair(root) else 'Nem teljesül')
        except Exception as e:
            raise Exception('Format could not be read\n' + str(e))