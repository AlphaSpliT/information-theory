# Testvérpár tulajdonság
Döntsük el egy bináris fáról, hogy esetében teljesül-e a testvérpár-tulajdonság.

## Bemenet
`érték (bal ág) (jobb ág)`
_ahol a bal és a jobb ág is ugyanilyen formátumú. (Egy ágat csak akkor írunk le, ha szükséges.)_ <br>
A bemenetet szöveges fájlban adjuk meg.

## Példák

### 1. példa:
> **Bemenet**: 10 (6 (4 (3) (1)) (2)) (4) <br/>
> **Kimenet**: Nem teljesül

### 2. példa:
> **Bemenet**: 11 (5 (3) (2 (1) (1))) (6 (5) (1)) <br/>
> **Kimenet**: Nem teljesül

### 3. példa:
> **Bemenet**: 11 (5 (3) (2 (1) (1))) (6 (3) (3)) <br/>
> **Kimenet**: Teljesül

### 4. példa:
> **Bemenet**: 10 (6 (3 (2) (1)) (3)) (4) <br/>
> **Kimenet**: Teljesül


## Usage
```bash
> python ex5.py <input-file>
```

## Example
```bash
> python ex5.py input.txt
Bemenet: 10 (6 (4 (3) (1)) (2)) (4)
Kimenet: Nem teljesül
Bemenet: 11 (5 (3) (2 (1) (1))) (6 (5) (1))
Kimenet: Nem teljesül
Bemenet: 11 (5 (3) (2 (1) (1))) (6 (3) (3))
Kimenet: Teljesül
Bemenet: 10 (6 (3 (2) (1)) (3)) (4)
Kimenet: Teljesül
```