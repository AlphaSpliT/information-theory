import argparse
from math import floor

def hamming_d(v1, v2):
    assert len(v1) == len(v2), "Only defined between same-length arrays"

    d = 0

    for i in range(len(v1)):
        d += 1 if v1[i] != v2[i] else 0

    return d

def d_min(codes):
    d_min = 9999
    for i in range(len(codes)):
        for j in range(i + 1, len(codes)):
            current = hamming_d(codes[i], codes[j])
            if current < d_min:
                d_min = current

    return d_min

parser = argparse.ArgumentParser(description='')

parser.add_argument('-f', '--file', action='append', required=True, help='Input file for codes')

args = parser.parse_args()


for input_file in args.file:
    with open(input_file, 'r') as f:
        codes = f.read().split('\n')
        codes = [code.strip() for code in codes if code]
    
    code_d_min = d_min(codes)
    print(input_file)
    print(f'Hibák észlelése: {code_d_min - 1}')
    print(f'Hibák javítása: {floor((code_d_min - 1) / 2)}')
    print('-----------------------------')
