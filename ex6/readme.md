# Hibajavítas

Adott egy bináris hibajavító kód, melyet egy bemeneti fájlból olvasunk be, minden sor egy kódszót tartalmaz. Írjunk programot, amely megmondja, hogy a kód hány hibát képes észlelni (jelezni), illetve javítani.

## Examples

### 1. példa:
> Bemenet: <br>
> 000 <br>
> 111 <br>
> Kimenet: <br>
> Hibák észlelése: **2** <br>
> Hibák javítása: **1** <br>

### 2. példa:
> Bemenet: <br>
> 000 <br>
> 011 <br>
> 101 <br>
> 110 <br>
> Kimenet: <br>
> Hibák észlelése: **1** <br>
> Hibák javítása: **0** <br>

### 3. példa:
> Bemenet: <br>
> 0000 <br>
> 0010 <br>
> 1001 <br>
> 1011 <br>
> Kimenet: <br>
> Hibák észlelése: **0** <br>
> Hibák javítása: **0** <br>

## Usage

```s
> python main.py --help
usage: main.py [-h] -f FILE

optional arguments:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Input file for codes
``` 

## Example usage

```s
❯ python3 main.py -f input3.txt -f input2.txt -f input1.txt
input3.txt
Hibák észlelése: 0
Hibák javítása: 0
-----------------------------
input2.txt
Hibák észlelése: 1
Hibák javítása: 0
-----------------------------
input1.txt
Hibák észlelése: 2
Hibák javítása: 1
-----------------------------
```