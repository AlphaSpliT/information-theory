import re
import os
import json
import pathlib

if __name__ == "__main__":
    def filter_function(word):
        return len(word) > 1 or word == 'a'

    file_list = os.listdir('./data/')
    count = 0
    size = 0
    for file_name in file_list:
        # print(os.path.abspath(f'./data/{file_name}'), '->', os.path.abspath(f'./clean/{file_name.split(".")[0]}.json'))
        with open(os.path.abspath(f'./data/{file_name}'), 'r') as f:
            text = f.read()
            language = re.findall('Language: .*',  text)[0].split(':')[1].strip().lower()
            if 'english' not in language:
                words = re.findall(r'[A-Za-z\']+', text)
                words = list(map(lambda word: word.lower(), words))
                words = list(filter(filter_function, words))
                pathlib.Path(f'./clean/{language}').mkdir(exist_ok=True, parents=True)
                with open(os.path.abspath(f'./clean/{language}/{file_name.split(".")[0]}.json'), 'w') as g:
                    g.write(json.dumps(words))
            else:
                print(language)