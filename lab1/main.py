import gzip
import json
import os
import random
import sys

import numpy as np
from nltk import ngrams


def H(X):
    return np.sum(np.log2(1 / X) * X)


language = 'english'
n_grams = 2
randselect = False

if len(sys.argv) == 1:
    print(f'Usage: {sys.argv[0]} <n_grams|number> <lanugage> --rand')

if len(sys.argv) >= 2:
    n_grams = int(sys.argv[1])

if len(sys.argv) >= 3:
    language = sys.argv[2]

if '--rand' in sys.argv:
    randselect = True

if __name__ == "__main__":
    print(f'Working with n_grams={n_grams} and language={language}')

    loaded = 0
    occurrences = {}
    count = 0
    width = 40
    last_shown = 0
    PATH = f'./clean/{language}'
    if randselect:
        files_to_load = random.sample(os.listdir(PATH), random.randint(25, 45))
    else:
        files_to_load = os.listdir(PATH)

    size_to_load = sum([os.path.getsize(os.path.join(PATH, file_to_load))
                        for file_to_load in files_to_load]) / 1024 / 1024
    print(f'Loading {size_to_load:0.2f}mb of data:\n[', end='')
    print(' ' * width + ']', end='')
    sys.stdout.write('\b' * (width + 1) + ']')
    sys.stdout.flush()

    for to_load in files_to_load:
        with open(os.path.join(PATH, to_load), 'r') as f:
            words = json.loads(f.read())[150:]
            ngrams_list = [ngram for ngram in ngrams(words, n_grams)]
            count += len(ngrams_list)
            for ngram in ngrams_list:
                if ngram in occurrences:
                    occurrences[ngram] += 1
                else:
                    occurrences[ngram] = 1

            loaded += os.path.getsize(os.path.join(PATH,
                                                   to_load)) / 1024 / 1024
            if last_shown < int((loaded / size_to_load) * (width - 1)):
                sys.stdout.write('\b')
                sys.stdout.flush()
                print('-' * (int((loaded / size_to_load) *
                                 (width - 1)) - last_shown) + '>', end='')
                last_shown = int((loaded / size_to_load) * (width - 1))
                sys.stdout.flush()

    print(']')
    print(f'Number of grams: {count:,}')
    print(f'Number of unique grams: {len(occurrences.keys()):,}')
    probabillities = np.array(
        [occurrence / count for occurrence in occurrences.values()])
    print(f'H(X) = {H(probabillities) / n_grams:0.2f}')
