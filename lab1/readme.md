# Entrópia számolás

## Függőségek

- numpy
- nltk

## Használat
A `downloader.sh`-t segítségével, paraméterként `english.txt` vagy `finnish.txt` használva letölthetjük a gutenberg adatbázisból az adott listában szereplő könyveket, ezek id-ja szerint.
Ezekután a `python clean.py` parancs szitálja illetve nyelv szerint osztályozza a letöltött adatokat, létrehozva egy `clean` nevű mappát, amelyben minden felismert nyelvnek létrehozódik egy almappája, amelyben json formátumban el vannak mentve a szitált szavak.
A `python main.py` 3 opcionális paramétert vár el:

1) gram méret
2) mappa (nyelv clean mappán belül)
3) --rand random shuffle

## Example
```bash
python main.py 3 english --rand

Working with n_grams=3 and language=english
Loading 12.04mb of data:
[--------------------------------------->]
Number of grams: 1,479,019
Number of unique grams: 1,081,583
H(X) = 6.48
```