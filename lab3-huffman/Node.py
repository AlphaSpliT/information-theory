class Node:
    left: 'Node' = None
    right: 'Node' = None
    parent: 'Node' = None
    value: int
    id: str

    def __cmp__(self, other: 'Node'):
        return (self.value > other.value) - (self.value < other.value)

    def __lt__(self, other):
        return self.value > other.value

    def __str__(self):
        return f'<Node id={list(map(ord, [*self.id]))} value={self.value}/>'
