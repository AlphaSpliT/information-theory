from queue import PriorityQueue
from typing import Optional

from Node import Node


class NodePriorityQueue(PriorityQueue):
    _size: int = 0

    def put(self, item: Node, block: bool = ..., timeout: Optional[float] = ...) -> None:
        super().put((item.value, item))
        self._size += 1

    def get(self, block: bool = ..., timeout: Optional[float] = ...) -> Node:
        if self._size >= 1:
            self._size -= 1
            return super().get()[1]

        return None

    def size(self) -> int:
        return self._size
