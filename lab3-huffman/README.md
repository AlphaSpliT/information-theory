# Adaptive Huffman coding
Encode and decode data using adaptive huffman coding with complete base tree.

## Requirements
- python3
- _linux_ is recommended

## Installation
- `python3 -m pip install -r requirements.txt`

## Usage
```bash
> python main.py -h
Usage: main.py [-d|-e] <input_file> <output_file>
        -d              Decode input_file file into output_file
        -e              Encode input_file file into output_file
        <input_file>    Name of file to read data from
        <output_file>   Name of file to write data to
```

## Examples

### Encode __/bin/ls__
```bash
> python3 main.py -e /bin/ls ./ls.enc
> du --bytes ./ls.enc
122311  ./ls.enc
```

### Decode __./ls.enc__
```bash
> python3 main.py -d ./ls.enc ./ls.dec
> du --bytes /bin/ls ./ls.dec
141936  /bin/ls
141936  ./ls.dec
# size matches ^_^
> chmod +x ./ls.dec
> ./ls.dec
example.in  ls.dec  ls.enc  main.py  NodePriorityQueue.py  Node.py  __pycache__  README.md  requirements.txt  Tree.py  venv
# executable works”
```