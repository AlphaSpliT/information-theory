from typing import List

from Node import Node
from NodePriorityQueue import NodePriorityQueue


class Tree:
    root: Node
    decoded_data: (Node, str)

    def __init__(self, root: Node):
        self.root = root
        self.decoded_data = (self.root, '')

    @staticmethod
    def swap(x: Node, y: Node):
        if x.parent.left == x:
            if y.parent.left == y:
                x.parent.left, y.parent.left = y, x
            else:
                x.parent.left, y.parent.right = y, x
        else:
            if y.parent.left == y:
                x.parent.right, y.parent.left = y, x
            else:
                x.parent.right, y.parent.right = y, x

        x.parent, y.parent = y.parent, x.parent
        list(map(Tree.update_ids, [x, y]))

    def encode_character(self, character: str):
        code, node = self.find_node(character)
        self.update_values(node, len(code))
        return code

    def update_values(self, node: Node, depth_limit: int):
        iterator_node = node
        while iterator_node is not None:
            node_to_swap_with = self.find_by_value(iterator_node.value, depth_limit)

            if node_to_swap_with is not None and node_to_swap_with != iterator_node:
                self.swap(iterator_node, node_to_swap_with)

            iterator_node.value += 1
            iterator_node = iterator_node.parent

    def decode_data(self, data: str):
        message = ''
        current_node, read_cursor = self.decoded_data
        for code in [*data]:
            if code == '0':
                current_node = current_node.left
            elif code == '1':
                current_node = current_node.right

            read_cursor += code

            if len(current_node.id) == 1:
                message += current_node.id
                self.update_values(current_node, len(read_cursor))
                read_cursor = ''
                current_node = self.root
        self.decoded_data = (current_node, read_cursor)

        return message

    def find_node(self, data: str) -> (str, Node):
        code = ''
        iterator_node = self.root

        while iterator_node.id != data:
            if data in iterator_node.left.id:
                code += '0'
                iterator_node = iterator_node.left
            elif data in iterator_node.right.id:
                code += '1'
                iterator_node = iterator_node.right
            else:
                raise Exception(f'Data is not present in tree: {data}')

        return code, iterator_node

    def find_by_value(self, value, max_depth):
        def _find_by_value(current_node, depth=1):
            if depth >= max_depth:
                return None

            if current_node.value == value:
                return current_node

            if current_node.left is not None:
                l_res = _find_by_value(current_node.left, depth + 1)
                if l_res is not None:
                    return l_res

            if current_node.right is not None:
                r_res = _find_by_value(current_node.right, depth + 1)
                if r_res is not None:
                    return r_res

            return None

        return _find_by_value(self.root.left) or _find_by_value(self.root.right)

    @staticmethod
    def update_ids(node: Node):
        iterator_node: Node = node.parent
        while iterator_node:
            iterator_node.id = f'{iterator_node.left.id}{iterator_node.right.id}'
            iterator_node = iterator_node.parent

    def __str__(self):
        def walk(root=self.root, depth=0):
            result = ''

            result += f'{"|  " * depth}{str(root)}\n'

            if root.right is not None:
                result += walk(root.right, depth + 1)

            if root.left is not None:
                result += walk(root.left, depth + 1)

            return result

        return walk()

    @classmethod
    def from_array(cls, array: List[str]):
        queue: NodePriorityQueue = NodePriorityQueue()

        def create_node(_id):
            node = Node()
            node.id = _id
            node.value = 1
            return node

        nodes = list(map(create_node, array))
        for node in nodes:
            queue.put(node)

        while queue.size() > 1:
            node = Node()
            node.left = queue.get()
            node.left.parent = node
            node.right = queue.get()
            node.right.parent = node
            node.value = node.right.value + node.left.value
            node.id = f'{node.left.id}{node.right.id}'
            queue.put(node)

        root = queue.get()
        return cls(root)