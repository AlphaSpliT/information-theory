import os
import struct
import sys

import bitstring

from Tree import Tree


def print_usage():
    print(f'Usage: {sys.argv[0]} [-d|-e] <input_file> <output_file>')
    print('\t-d\t\tDecode input_file file into output_file')
    print('\t-e\t\tEncode input_file file into output_file')
    print('\t<input_file>\tName of file to read data from')
    print('\t<output_file>\tName of file to write data to')


if __name__ == '__main__':
    if '-h' or '--help' in sys.argv:
        print_usage()
        exit(0)
        
    if '-d' not in sys.argv and '-e' not in sys.argv:
        print_usage()
        print('-d or -e must be present', file=sys.stderr)
        exit(1)

    if '-d' in sys.argv and '-e' in sys.argv:
        print_usage()
        print('-d or -e must be present exclusively', file=sys.stderr)
        exit(1)

    if len(sys.argv) != 4:
        print_usage()
        exit(1)

    mode = 'encode' if '-e' in sys.argv else 'decode'
    input_file = sys.argv[2]
    output_file = sys.argv[3]

    dictionary = [chr(i) for i in range(256)]
    tree = Tree.from_array(dictionary)

    if mode == 'encode':
        original_size = os.path.getsize(input_file)
        with open(output_file, 'wb') as g:
            g.write(struct.pack('I', original_size))
            with open(input_file, 'rb') as f:
                buf = f.read(1)
                codes = ''
                while buf:
                    codes += tree.encode_character(chr(ord(buf)))

                    while len(codes) >= 8:
                        byte = bitstring.BitArray(bin=codes[:8])
                        byte.tofile(g)
                        codes = codes[8:]

                    buf = f.read(1)

                if len(codes) != 0:
                    byte = bitstring.BitArray(bin=codes)
                    byte.tofile(g)

    elif mode == 'decode':
        with open(input_file, 'rb') as f:
            original_size = struct.unpack('I', f.read(4))[0]
            print(original_size)
            count = 0
            with open(output_file, 'wb') as g:
                buf = f.read(1)
                while buf and count < original_size:
                    bit_stream = bitstring.BitArray(bytes=buf)
                    for bit in bit_stream.bin:
                        message = tree.decode_data(bit)
                        if message:
                            count += len(message)
                            g.write(bytes([ord(message)]))
                            if count >= original_size:
                                break

                    buf = f.read(1)
