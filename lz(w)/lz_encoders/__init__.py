import os
import sys

import os
import sys

if os.environ.get('READTHEDOCS') == 'True' or sys.argv[0] == 'completion.py':
    from .mock import (
        encode_lz78, decode_lz78
    )

else:
    from .lz_encoders import (
        encode_lz78, decode_lz78, encode_lz78_string, decode_lz78_string,
        encode_lzw, decode_lzw, encode_lzw_string, decode_lzw_string,
        encode_lz77, decode_lz77,  encode_lz77_string, decode_lz77_string,
    )


__all__ = [
    'encode_lz78', 'decode_lz78', 'encode_lz78_string', 'decode_lz78_string',
    'encode_lzw', 'decode_lzw', 'encode_lzw_string', 'decode_lzw_string',
    'encode_lz77', 'decode_lz77', 'encode_lz77_string', 'decode_lz77_string',
]

__version__ = '1.0.0'