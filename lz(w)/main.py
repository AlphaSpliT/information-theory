import lz_encoders
import argparse


parser = argparse.ArgumentParser(description='Encode and decode data using LZ77, LZ78 or LZW methods')

encode_decode_group = parser.add_mutually_exclusive_group(required=True)
encode_decode_group.add_argument('-e', '--encode', action='store_true', help='encode given data')
encode_decode_group.add_argument('-d', '--decode', action='store_true', help='decode given data')

parser.add_argument('-m', '--method', choices=['lz77', 'lz78', 'lzw'], action='store', required=True, help='Method used for encoding/decoding')
parser.add_argument('-t', '--type', choices=['file', 'string'], action='store', required=True, help='Should input be text or file')
parser.add_argument('-if', '--input-file', action='store', required=False)
parser.add_argument('-of', '--output-file', action='store', required=False)
parser.add_argument('-it', '--input-text', action='store', required=False)
parser.add_argument('--swsize', action='store', required=False, type=int, help='Search window size')
parser.add_argument('--fwsize', action='store', required=False, type=int, help='Forward window size')

args = parser.parse_args()

if args.encode:
    if args.type == 'file':
        if args.input_file is None or args.output_file is None:
            parser.error('--type file requires -if/--input-file and -of/--output-file')
    elif args.type == 'string':
        if args.input_text is None:
             parser.error('--type string requires -it/--input-text')

    if args.method == 'lz77':
        if args.swsize is None or args.fwsize is None:
            parser.error('--encode --method lz77 requires --swsize and --fwsize')
elif args.decode:
    if args.type == 'file':
        if args.input_file is None or args.output_file is None:
            parser.error('--type file requires -if/--input-file and -of/--output-file')
    elif args.type == 'string':
        parser.error('decoding in string type is not supported yet')

if args.encode:
    if args.type == 'file':
        input_file = args.input_file
        output_file = args.output_file
        
        if args.method == 'lz77':
            h_k = args.swsize
            h_e = args.fwsize
            lz_encoders.encode_lz77(input_file, output_file, h_k, h_e)
        elif args.method == 'lz78':
            lz_encoders.encode_lz78(input_file, output_file)
        elif args.method == 'lzw':
            lz_encoders.encode_lzw(input_file, output_file)
    elif args.type == 'string':
        input_text = args.input_text
        if args.method == 'lz77':
            h_k = args.swsize
            h_e = args.fwsize
            result = lz_encoders.encode_lz77_string(input_text, h_k, h_e);
            for dct in result:
                print('(', end='')
                for key in dct.keys():
                    print(str(key) + '=' + str(dct[key]), end=', ')
                print(')')
        elif args.method == 'lz78':
            result = lz_encoders.encode_lz78_string(input_text)
            for dct in result:
                print('(', end='')
                for key in dct.keys():
                    print(str(key) + '=' + str(dct[key]), end=', ')
                print(')')
        elif args.method == 'lzw':
            result = lz_encoders.encode_lzw_string(input_text)
            for dct in result:
                print('(', end='')
                for key in dct.keys():
                    print(str(key) + '=' + str(dct[key]), end=', ')
                print(')')
elif args.decode:
     if args.type == 'file':
        input_file = args.input_file
        output_file = args.output_file
        
        if args.method == 'lz77':
            h_k = args.swsize
            h_e = args.fwsize
            lz_encoders.decode_lz77(input_file, output_file)
        elif args.method == 'lz78':
            lz_encoders.decode_lz78(input_file, output_file)
        elif args.method == 'lzw':
            lz_encoders.decode_lzw(input_file, output_file)