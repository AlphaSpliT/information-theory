# LZ Encoders
Encode and decode data using LZ77, LZ78 or LZW methods

## Requirements
- python3
- _linux_ is recomended

## Installation
- `python3 -m pip install -r requirements.txt`
- `chmod +x ./install.sh && ./install.sh`

## Usage
```bash
> python main.py -h
usage: main.py [-h] (-e | -d) -m {lz77,lz78,lzw} -t {file,string}
               [-if INPUT_FILE] [-of OUTPUT_FILE] [-it INPUT_TEXT]
               [--swsize SWSIZE] [--fwsize FWSIZE]

Encode and decode data using LZ77, LZ78 or LZW methods

optional arguments:
  -h, --help            show this help message and exit
  -e, --encode          encode given data
  -d, --decode          decode given data
  -m {lz77,lz78,lzw}, --method {lz77,lz78,lzw}
                        Method used for encoding/decoding
  -t {file,string}, --type {file,string}
                        Should input be text or file
  -if INPUT_FILE, --input-file INPUT_FILE
  -of OUTPUT_FILE, --output-file OUTPUT_FILE
  -it INPUT_TEXT, --input-text INPUT_TEXT
  --swsize SWSIZE       Search window size
  --fwsize FWSIZE       Forward window size
```

## Examples

### Encode __balalajka__ with lz78
```bash
> python3 main.py -e -t string -m lz78 -it="balalajka"
(index=0, character=98, )
(index=0, character=97, )
(index=0, character=108, )
(index=2, character=108, )
(index=2, character=106, )
(index=0, character=107, )
(index=2, character=256, )
```

### Encode __balalajka__ with lz77 search window size 5 and forwad window size 3
```bash
> python3 main.py -e -t string -m lz77 -it="balalajka" --swsize 5 --fwsize 3
(index=0, length=0, character=98, )
(index=0, length=0, character=97, )
(index=0, length=0, character=108, )
(index=2, length=2, character=97, )
(index=0, length=0, character=106, )
(index=0, length=0, character=107, )
(index=5, length=1, character=256, )
```

### Encode __balalajka__ with lzw
```bash
> python3 main.py -e -t string -m lzw -it="balalajka"
(index=99, )
(index=98, )
(index=109, )
(index=258, )
(index=98, )
(index=107, )
(index=108, )
(index=98, )
```

### Encode __/bin/ls__ with lz(w)
```bash
> python3 main.py -e -t file -m lzw -if=/bin/ls -of=ls.enc
> du --bytes ./ls.enc
183672  ./ls.enc
```

### Decode __./ls.enc__ with lz(w)
```bash
> python3 main.py -d -t file -m lzw -if=ls.enc -of=ls.dec
> du --bytes /bin/ls ./ls.dec
138856  /bin/ls
138856  ./ls.dec
# size matches ✔
> chmod +x ./ls.dec
> ./ls.dec
.  ..  build  .gitignore  install.sh  ls.dec  ls.enc  lz_encoders  main.py  MANIFEST.in  readme.md  requirements.txt  setup.py  src
# executable works ✔
```