from setuptools import setup, Extension

lz_encoders = Extension(
    'lz_encoders',
    sources=[
        'src/lz_encoders/Utils.cpp',
        'src/lz_encoders/DataChunk.cpp',
        'src/lz_encoders/PrefixTree.cpp',
        'src/lz_encoders/LZ77.cpp',
        'src/lz_encoders/LZ78.cpp',
        'src/lz_encoders/LZW.cpp',        
        'src/module.cpp',
        'src/others.cpp'
    ]
)

setup(
    name='lz_encoders.lz_encoders',
    version='1.0.0',
    packages = ['lz_encoders'],
    ext_modules=[lz_encoders]
)