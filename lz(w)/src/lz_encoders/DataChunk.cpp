#include "DataChunk.hpp"

void DataChunk::add_byte(const unsigned char &byte) {
    this->data_holder.push_back(byte);
}

size_t DataChunk::size() const {
    return this->data_holder.size();
}

DataChunk DataChunk::slice(const size_t &start_pos, const size_t &end_pos) const {
    if (start_pos < 0 || start_pos > this->data_holder.size() ||
        end_pos < start_pos || end_pos < 0 || end_pos > this->data_holder.size()) {
        throw std::range_error("Invalid slice range");
    }

    std::vector<unsigned char> result(this->data_holder.begin() + start_pos, this->data_holder.begin() + end_pos);

    return DataChunk(result);
}

int DataChunk::find(const DataChunk &that) const {
    if (that.size() > this->size()) {
        return -1;
    }

    size_t match_index = 0;
    int initial_match_index = -1;
    size_t iterator = 0;
    

    while (iterator < this->size()) {
        if (this->data_holder[iterator] == that.data_holder[match_index]) {
            match_index++;
            if (initial_match_index == -1) {
                initial_match_index = iterator;
            }
        } else {
            match_index = 0;
            initial_match_index = -1;
        }

        if (match_index == that.size()) {
            return initial_match_index;
        }

        iterator++;
    }

    if (match_index == that.size()) {
        return initial_match_index;
    }

    return -1;
}

unsigned char *DataChunk::get_buffer() const {
    unsigned char *buffer = new unsigned char[this->data_holder.size()];
    memcpy(buffer, &this->data_holder[0], this->data_holder.size());

    return buffer;
}

bool DataChunk::operator==(const DataChunk &that) const {
    if (that.data_holder.size() != this->data_holder.size()) {
        return false;
    }

    return std::equal(that.data_holder.begin(),
                      that.data_holder.end(),
                      this->data_holder.begin(),
                      this->data_holder.end());
}

unsigned char DataChunk::operator[](const size_t &index) const {
    if (index >= this->data_holder.size()) {
        throw std::range_error("Out of range");
    }

    return this->data_holder[index];
}

std::string DataChunk::print_buffer() const {
    std::stringstream ss;
    for (unsigned char character: this->data_holder) {
        ss << (short)character << ",";
    }

    return ss.str();
}

DataChunk DataChunk::operator+=(unsigned char character) {
    this->data_holder.push_back(character);

    return *this;
}

DataChunk DataChunk::operator+=(const DataChunk &that) {
    std::copy(that.data_holder.begin(), that.data_holder.end(), std::back_inserter(this->data_holder));

    return *this;
}

DataChunk DataChunk::operator+(unsigned char character) {
    DataChunk result;
    std::copy(this->data_holder.begin(), this->data_holder.end(), std::back_inserter(result.data_holder));
    result.data_holder.push_back(character);

    return result;
}

DataChunk DataChunk::operator+(const DataChunk &that) {
    DataChunk result;
    std::copy(this->data_holder.begin(), this->data_holder.end(), std::back_inserter(result.data_holder));
    std::copy(that.data_holder.begin(), that.data_holder.end(), std::back_inserter(result.data_holder));

    return result;
}

DataChunk::DataChunk(const std::vector<unsigned char> &data) {
    this->data_holder = std::vector<unsigned char>(data.begin(), data.end());
}

DataChunk::DataChunk() {
}

DataChunk::DataChunk(unsigned char character) {
    this->data_holder.push_back(character);
}

DataChunk::DataChunk(unsigned char *buffer, const size_t &size) {
    this->data_holder.clear();
    std::copy(buffer, buffer + size, std::back_inserter(this->data_holder));
}