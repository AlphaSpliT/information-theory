#ifndef DATACHUNK_HPP
#define DATACHUNK_HPP

#include <algorithm>
#include <cstring>
#include <iostream>
#include <stdexcept>
#include <vector>
#include <sstream>

class DataChunk {
   private:
    std::vector<unsigned char> data_holder;

   public:
    DataChunk();
    DataChunk(unsigned char);
    DataChunk(const std::vector<unsigned char>&);
    DataChunk(unsigned char *, const size_t &);
    void add_byte(const unsigned char &);
    size_t size() const;
    DataChunk slice(const size_t &, const size_t &) const;
    bool operator==(const DataChunk &) const;
    unsigned char operator[](const size_t &) const;
    DataChunk operator+=(unsigned char);
    DataChunk operator+=(const DataChunk &);
    int find(const DataChunk &) const;

    unsigned char *get_buffer() const;

    DataChunk operator+(unsigned char);
    DataChunk operator+(const DataChunk &);

    std::string print_buffer() const;
};

#endif