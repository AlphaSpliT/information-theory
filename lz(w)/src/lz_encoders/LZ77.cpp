#include "LZ77.hpp"

void LZ77::encode(const std::string& input_file, const std::string& output_file, const size_t& h_k, const size_t& h_e) {
    std::streamsize size;
    unsigned char* binary_data = read_contents_binary(input_file.c_str(), size);

    std::vector<SaveStructure> save_holder = encode_data(binary_data, size, h_k, h_e);

    ActualSaveStructure save_structure;
    save_structure.h_e = h_e;
    save_structure.h_k = h_k;
    save_structure.number_of_structures = save_holder.size();
    save_structure.raw_data = (unsigned char*)&save_holder[0];

    std::ofstream outfile(output_file, std::ios::binary);
    outfile.write((char*)&save_structure, sizeof(ActualSaveStructure) - sizeof(unsigned char*));
    outfile.write((char*)save_structure.raw_data, sizeof(SaveStructure) * save_structure.number_of_structures);
    outfile.close();
}


DataChunk LZ77::decode_data(const ActualSaveStructure& data) {
    std::vector<SaveStructure> save_holder((SaveStructure *)data.raw_data, ((SaveStructure *)data.raw_data) + data.number_of_structures);
    size_t h_k = data.h_k;

    DataChunk result;

    for (const auto& saved: save_holder) {
        DataChunk s_w = result.size() > h_k ? result.slice(result.size() - h_k, result.size()) : result;
        if (saved.start_index != 0) {
            size_t index = s_w.size() - saved.start_index;
            result += s_w.slice(index, index + saved.length);
        }

        if (saved.character != 256) {
            result += saved.character;
        } 
    }

    return result;
}

std::vector<LZ77::SaveStructure> LZ77::encode_data(unsigned char* data, const size_t& size, const size_t& h_k, const size_t& h_e) {
    DataChunk remaining(data, size);
    
    DataChunk processed;
    std::vector<SaveStructure> save_holder;

    while (remaining.size() != 0) {
        DataChunk s_w = processed.size() >= h_k ? processed.slice(processed.size() - h_k, processed.size()) : processed;
        DataChunk f_w = remaining.size() > h_e ? remaining.slice(0, h_e) : remaining;
    
        auto possible_matches = generate_possible_matches(f_w);
        bool matched = false;
        
        for (const auto& possible_match : possible_matches) {
            int index = s_w.find(possible_match);
            size_t match_size = possible_match.size();
            if (index != -1) {
                processed += possible_match;
                remaining = remaining.size() > match_size ? remaining.slice(match_size, remaining.size()) : DataChunk();

                if (remaining.size()) {
                    save_holder.push_back(SaveStructure(s_w.size() - index, match_size, remaining[0]));
                    processed += remaining[0];
                } else {
                    save_holder.push_back(SaveStructure(s_w.size() - index, match_size, 256));
                }

                remaining = remaining.size() != 0 ? remaining.slice(1, remaining.size()) : DataChunk();
                matched = true;
                break;
            }
        }

        if (!matched) {
            if (remaining.size() != 0) {
                processed += remaining[0];
                save_holder.push_back(SaveStructure(0, 0, remaining[0]));
                remaining = remaining.size() != 0 ? remaining.slice(1, remaining.size()) : DataChunk();
            } else {
                save_holder.push_back(SaveStructure(0, 0, 256));
            }
        }
    }

    return save_holder;
}

std::vector<DataChunk> LZ77::generate_possible_matches(const DataChunk& f_w) {
    std::vector<DataChunk> result;
    size_t size_of_window = f_w.size();

    for (size_t i = 1; i < size_of_window + 1; ++i) {
        result.push_back(f_w.slice(0, i));
    }

    std::reverse(result.begin(), result.end());
    
    return result;
}

void LZ77::decode(const std::string& input_file, const std::string& output_file) {
    std::streamsize size;
    unsigned char* read_data = read_contents_binary(input_file.c_str(), size);
    
    ActualSaveStructure saved_structure;
    memcpy(&saved_structure.h_k, read_data, sizeof(size_t));
    memcpy(&saved_structure.h_e, &read_data[8], sizeof(size_t));
    memcpy(&saved_structure.number_of_structures, &read_data[16], sizeof(size_t));
    saved_structure.raw_data = &read_data[24];

    auto result = decode_data(saved_structure);

    std::ofstream outfile(output_file, std::ios::binary);
    outfile.write((char *)result.get_buffer(), result.size());
    outfile.close();
}