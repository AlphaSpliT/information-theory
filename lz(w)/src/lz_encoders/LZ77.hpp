#ifndef LZ77_HPP
#define LZ77_HPP

#include <string>
#include <vector>

#include "DataChunk.hpp"
#include "Utils.hpp"

namespace LZ77 {
struct SaveStructure {
    SaveStructure(const int& start_index, const int& length, const short& character) {
        this->start_index = start_index;
        this->length = length;
        this->character = character;
    }

    int start_index;
    int length;
    short character;
};

struct ActualSaveStructure {
    size_t h_k;
    size_t h_e;
    size_t number_of_structures;
    unsigned char* raw_data;
};

std::vector<SaveStructure> encode_data(unsigned char*, const size_t&, const size_t&, const size_t&);
DataChunk decode_data(const ActualSaveStructure&);

void encode(const std::string& input_file, const std::string& output_file, const size_t& h_k, const size_t& h_e);
void decode(const std::string& input_file, const std::string& output_file);
std::vector<DataChunk> generate_possible_matches(const DataChunk& f_w);
}  // namespace LZ77

#endif