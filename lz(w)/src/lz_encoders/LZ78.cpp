#include "LZ78.hpp"

#include "DataChunk.hpp"

void LZ78::encode(const char *input_file, const char *output_file) {
    std::streamsize size;
    unsigned char *binary_data = read_contents_binary(input_file, size);
    std::vector<SaveStructure> save_holder = encode_data(binary_data, size);

    std::ofstream outfile(output_file, std::ios::binary);
    outfile.write((char *)&save_holder[0], sizeof(SaveStructure) * save_holder.size());
    outfile.close();
}

void LZ78::decode(const char *input_file, const char *output_file) {
    std::streamsize size;
    SaveStructure *saved_structure = (SaveStructure *)read_contents_binary(input_file, size);
    size_t number_of_structures = size / sizeof(SaveStructure);

    std::vector<SaveStructure> saved_data_vector(saved_structure, saved_structure + number_of_structures);

    DataChunk result = decode_data(saved_data_vector);
    unsigned char *buffer = result.get_buffer();

    std::ofstream outfile(output_file, std::ios::binary | std::ios::ate);
    outfile.write((char *)buffer, result.size());
    outfile.close();
}

std::vector<LZ78::SaveStructure> LZ78::encode_data(unsigned char *data, const size_t &size) {
    Node *root = new Node(0);
    PrefixTree prefix_tree(root);

    DataChunk remaining(data, size);

    std::vector<SaveStructure> save_holder;

    while (remaining.size() != 0) {
        auto match_data = prefix_tree.match(remaining);
        auto longest_match_length = match_data.matched_data.size();

        if (longest_match_length != 0) {
            auto longest_match = match_data.matched_data;

            if (remaining.size() > longest_match_length) {
                auto to_push = remaining[longest_match_length];
                save_holder.push_back(SaveStructure(match_data.matched_index, to_push));
                prefix_tree.add_character_to_node(match_data.node, to_push);
            } else {
                save_holder.push_back(SaveStructure(match_data.matched_index, 256));
            }

            remaining = longest_match_length + 1 < remaining.size() ? remaining.slice(longest_match_length + 1, remaining.size()) : DataChunk();
        } else {
            save_holder.push_back(SaveStructure(0, remaining[0]));
            prefix_tree.add_character_to_root(remaining[0]);
            remaining = remaining.slice(1, remaining.size());
        }
    }

    return save_holder;
}

DataChunk LZ78::decode_data(const std::vector<SaveStructure> &saved_data) {
    std::vector<DataChunk> dictionary;
    DataChunk result;

    for (size_t i = 0; i < saved_data.size(); ++i) {
        auto pair = saved_data[i];
        auto character = pair.character;

        if (pair.index == 0) {
            dictionary.push_back(DataChunk(character));
            result += character;
        } else {
            DataChunk to_append;
            if (character == 256) {
                to_append = dictionary[pair.index - 1];
            } else {
                to_append = dictionary[pair.index - 1] + character;
            }

            dictionary.push_back(to_append);
            result += to_append;
        }
    }

    return result;
}