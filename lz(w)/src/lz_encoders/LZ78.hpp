#ifndef LZ78_HPP
#define LZ78_HPP

#include <string>
#include <vector>

#include "PrefixTree.hpp"

namespace LZ78 {
struct SaveStructure {
    unsigned int index;
    short character;

    SaveStructure() {}

    SaveStructure(const size_t &index, short character) {
        this->index = index;
        this->character = character;
    }
};

void encode(const char *input_file, const char *output_file);
void decode(const char *input_file, const char *output_file);
std::vector<SaveStructure> encode_data(unsigned char*, const size_t&);
DataChunk decode_data(const std::vector<SaveStructure>&);
}  // namespace LZ78

#endif
