#include "LZW.hpp"

void LZW::encode(const std::string &input_file, const std::string &output_file) {
    std::streamsize size;
    unsigned char *data = read_contents_binary(input_file.c_str(), size);
    std::vector<SaveStructure> save_holder = encode_data(data, size);

    std::ofstream outfile(output_file, std::ios::binary);
    outfile.write((char *)&save_holder[0], save_holder.size() * sizeof(SaveStructure));
    outfile.close();
}

void LZW::decode(const std::string &input_file, const std::string &output_file) {
    std::streamsize size;
    SaveStructure *saved_structure = (SaveStructure *)read_contents_binary(input_file.c_str(), size);
    size_t number_of_structures = size / sizeof(SaveStructure);

    std::vector<SaveStructure> saved_data_vector(saved_structure, saved_structure + number_of_structures);
    DataChunk result = decode_data(saved_data_vector);
    
    std::ofstream outfile(output_file, std::ios::binary);
    outfile.write((char *)result.get_buffer(), result.size());
    outfile.close();
}

PrefixTree LZW::build_prefixtree() {
    Node *root = new Node(0);
    PrefixTree prefix_tree(root);

    for (unsigned char c = 0; c < 255; ++c) {
        prefix_tree.add_character_to_root(c);
    }

    prefix_tree.add_character_to_root(255);

    return prefix_tree;
}

std::vector<DataChunk> LZW::build_prefixtree_simple() {
    std::vector<DataChunk> result;

    for (unsigned char c = 0; c < 255; ++c) {
        result.push_back(c);
    }

    result.push_back(255);

    return result;
}

std::vector<LZW::SaveStructure> LZW::encode_data(unsigned char *data, const size_t &size) {
    PrefixTree prefix_tree = build_prefixtree();
    DataChunk remaining(data, size);

    std::vector<SaveStructure> save_holder;

    while (remaining.size() != 0) {
        auto match_data = prefix_tree.match(remaining);
        auto longest_match_length = match_data.matched_data.size();
        save_holder.push_back(SaveStructure(match_data.matched_index));

        if (longest_match_length < remaining.size()) {
            prefix_tree.add_character_to_node(match_data.node, remaining[longest_match_length]);
        }

        remaining = longest_match_length < remaining.size() ? remaining.slice(longest_match_length, remaining.size()) : DataChunk();
    }

    return save_holder;
}

DataChunk LZW::decode_data(const std::vector<SaveStructure> &saved_data) {
    std::vector<DataChunk> dictionary = build_prefixtree_simple();
    DataChunk result;

    auto w = dictionary[saved_data[0].index - 1];
    result += w;

    for (size_t k = 1; k < saved_data.size(); k++) {
        auto index = saved_data[k].index - 1;
        DataChunk entry;

        if (index < dictionary.size()) {
            entry = dictionary[index];
        } else if (index == dictionary.size()) {
            entry = w + w[0];
        }

        result += entry;

        dictionary.push_back(w + entry[0]);
        w = entry;
    }
    
    return result;
}