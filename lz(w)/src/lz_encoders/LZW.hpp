#ifndef LZW_HPP
#define LZW_HPP

#include <fstream>
#include <string>
#include <vector>

#include "PrefixTree.hpp"
#include "Utils.hpp"

namespace LZW {
struct SaveStructure {
    unsigned int index;

    SaveStructure() {}

    SaveStructure(const size_t &index) {
        this->index = index;
    }
};

void encode(const std::string &input_file, const std::string &output_file);
void decode(const std::string &input_file, const std::string &output_file);
std::vector<SaveStructure> encode_data(unsigned char*, const size_t&);
DataChunk decode_data(const std::vector<SaveStructure>&);
PrefixTree build_prefixtree();
std::vector<DataChunk> build_prefixtree_simple();
}  // namespace LZW

#endif