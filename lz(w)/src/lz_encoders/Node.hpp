#ifndef NODE_HPP
#define NODE_HPP

#include <map>

struct Node
{
    Node(const size_t &index)
    {
        this->index = index;
    }

    size_t index;
    std::map<unsigned char, Node *> children;
};

#endif