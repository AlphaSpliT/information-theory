#include "PrefixTree.hpp"

PrefixTree::PrefixTree(Node *root)
{
    this->root = root;
    this->root->index = 0;
    this->index = 1;
}

PrefixTree::~PrefixTree()
{
}

MatchData PrefixTree::match(const DataChunk &to_match)
{
    Node *current_root = this->root;
    size_t count = 0;
    DataChunk result;

    while (count < to_match.size())
    {
        std::vector<unsigned char> keys = extract_keys(current_root->children);
        unsigned char current_character = to_match[count];
        bool found = std::find(keys.begin(), keys.end(), current_character) != keys.end();

        if (found)
        {
            count++;
            result += current_character;
            current_root = current_root->children.at(current_character);
        }
        else
        {
            break;
        }
    }

    MatchData match_data;
    match_data.matched_data = result;
    match_data.matched_index = current_root->index;
    match_data.node = current_root;

    return match_data;
}

void PrefixTree::add_character_to_node(Node *to_match, unsigned char character)
{

    Node *new_node = new Node(this->index++);
    to_match->children.insert(std::pair<char, Node *>(character, new_node));
}

void PrefixTree::add_character_to_root(unsigned char character)
{
    this->add_character_to_node(this->root, character);
}

void PrefixTree::walk()
{
    this->_walk(this->root, std::vector<unsigned char>());
}

void PrefixTree::_walk(Node *root, std::vector<unsigned char> buffer)
{
    std::vector<char unsigned> keys = extract_keys(root->children);

    if (keys.size() != 0)
    {
        for (auto &key : keys)
        {
            buffer.push_back(key);
            this->_walk(root->children.at(key), buffer);
            buffer.pop_back();
        }
    }
    else
    {
        for (auto &character : buffer)
        {
            std::cout << (short)character << " ";
        }
        std::cout << std::endl;
    }
}

size_t PrefixTree::get_index() const
{
    return this->index;
}