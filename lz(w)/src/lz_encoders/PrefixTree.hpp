#ifndef PREFIXTREE_HPP
#define PREFIXTREE_HPP

#include <algorithm>
#include <exception>
#include <iostream>

#include "DataChunk.hpp"
#include "Node.hpp"
#include "Utils.hpp"

struct MatchData {
    DataChunk matched_data;
    size_t matched_index;
    Node *node;
};

class PrefixTree {
   private:
    Node *root;
    size_t index;
    void _walk(Node *, std::vector<unsigned char>);

   public:
    PrefixTree(Node *);
    ~PrefixTree();

    MatchData match(const DataChunk &to_match);
    void add_character_to_node(Node *node, unsigned char character);
    void add_character_to_root(unsigned char character);
    void walk();
    size_t get_index() const;
};

#endif