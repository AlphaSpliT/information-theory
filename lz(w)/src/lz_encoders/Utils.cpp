#include "Utils.hpp"

std::vector<unsigned char> extract_keys(std::map<unsigned char, Node *> const &input_map) {
    std::vector<unsigned char> retval;
    for (auto const &element : input_map) {
        retval.push_back(element.first);
    }
    return retval;
}

int index_of(const std::vector<DataChunk> &input_list, const DataChunk &element) {
    auto it = std::find(input_list.begin(), input_list.end(), element);

    return it == input_list.end() ? -1 : std::distance(input_list.begin(), it);
}

DataChunk read_contents(const char *file_name) {
    std::streamsize size;
    unsigned char *buffer = read_contents_binary(file_name, size);
    return DataChunk(buffer, size);
}

unsigned char *read_contents_binary(const char *filename) {
    std::streamsize ssize;
    return read_contents_binary(filename, ssize);
}

unsigned char *read_contents_binary(const char *filename, std::streamsize &size) {
    
    if (!file_exists(filename)) {
        std::stringstream ss;
        ss << "File " << filename << " does not exist!" ;
        throw std::logic_error(ss.str());
    }

    std::ifstream file(filename, std::ios::binary | std::ios::ate);
    size = file.tellg();
    file.seekg(0, std::ios::beg);

    unsigned char *buffer = new unsigned char[size];

    if (!file.read((char *)buffer, size)) {
        throw std::logic_error("Cannot read from file...");
    }

    return buffer;
}

char hexchar_to_char(char hexchar) {
    if (hexchar >= '0' && hexchar <= '9')
        return hexchar - '0';
    if (hexchar >= 'A' && hexchar <= 'F')
        return hexchar - 'A' + 10;
    if (hexchar >= 'a' && hexchar <= 'f')
        return hexchar - 'a' + 10;
    throw std::invalid_argument("Invalid input string");
}

char *hexstring_to_char_array(std::string hex_string) {
    char *buffer = new char[hex_string.size() / 2];

    for (size_t i = 0; i < hex_string.size(); i += 2) {
        buffer[(int)i / 2] = hexchar_to_char(hex_string[i]) * 16 + hexchar_to_char(hex_string[i + 1]);
    }

    return buffer;
}

inline bool file_exists (const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}