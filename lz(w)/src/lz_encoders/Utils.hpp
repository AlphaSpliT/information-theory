#ifndef UTILS_HPP
#define UTILS_HPP

#include <algorithm>
#include <exception>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>
#include <sys/stat.h>

#include "DataChunk.hpp"
#include "Node.hpp"

std::vector<unsigned char> extract_keys(std::map<unsigned char, Node *> const &input_map);
int index_of(const std::vector<DataChunk> &input_list, const DataChunk &element);

DataChunk read_contents(const char *);
unsigned char *read_contents_binary(const char *, std::streamsize &);
unsigned char *read_contents_binary(const char *);

char hexchar_to_char(char);
char *hexstring_to_char_array(std::string);
inline bool file_exists (const std::string&);

#endif