#!/bin/bash
outputDirName='output'
mkdir -p ./$outputDirName
g++ -std=c++17 DataChunk.cpp Utils.cpp PrefixTree.cpp LZ77.cpp LZ78.cpp LZW.cpp main.cpp -o ./$outputDirName/main
