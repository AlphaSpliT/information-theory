#include "DataChunk.hpp"
#include "LZ78.hpp"
#include "LZ77.hpp"
#include "LZW.hpp"
#include <sys/stat.h>

size_t get_file_size(const std::string& filename) {
    struct stat st;
    if(stat(filename.c_str(), &st) != 0) {
        return 0;
    }
    return st.st_size;   
}

int main(int argc, char **argv) {
    // std::vector<LZW::SaveStructure> encoded_data = LZW::encode_data((unsigned char*) reinterpret_cast<const unsigned char *>("AAAAAA"), 5);
    // DataChunk decoded = LZW::decode_data(encoded_data);
    // std::cout << "-------------------------\n";
    
    // std::cout << "encoding with lz77\n";
    // LZ77::encode("test", "test.enc", 5, 3);
    // std::cout << "encoded size: " << get_file_size("test.enc") << " decoding with lz77\n";
    // LZ77::decode("test.enc", "test.dec");
    // std::cout << "original: " << get_file_size("test") << " after decoding: " << get_file_size("test.dec") << "\n";
    
    std::cout << "-------------------------\n";
    
    std::cout << "encoding with lz78\n";
    LZ78::encode("test", "test.enc");
    std::cout << "encoded size: " << get_file_size("test.enc") << " decoding with lz78\n";
    LZ78::decode("test.enc", "test.dec");
    std::cout << "original: " << get_file_size("test") << " after decoding: " << get_file_size("test.dec") << "\n";
    
    // std::cout << "-------------------------\n";

    // std::cout << "encoding with lz(w)\n";
    // LZW::encode("test", "test.enc");
    // std::cout << "encoded size: " << get_file_size("test.enc") << " decoding with lz(w)\n";
    // LZW::decode("test.enc", "test.dec");
    // std::cout << "original: " << get_file_size("test") << " after decoding: " << get_file_size("test.dec") << "\n";
    // std::cout << "-------------------------\n";


    // unsigned char data[5] {
    //     97, 108, 97, 106, 107
    // };

    // DataChunk data_chunk(data, 5);
    // DataChunk to_find('a');
    // std::cout << data_chunk.print_buffer() << '\n';
    // // std::cout << to_find.print_buffer() << '\n';
    // std::cout << data_chunk.find(to_find) << '\n';
    // auto result = LZ77::generate_possible_matches(DataChunk((unsigned char*) "bal", 3));
    // for (auto res: result) {
    //     std::cout << res.print_buffer() << "\n";
    // }
    // std::cout << decoded.get_buffer() << '\n';
    // LZW::encode("/usr/bin/sudo", "sudo.enc");
    // LZW::decode("sudo.enc", "sudo.dec");
}