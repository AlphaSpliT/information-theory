#include "common.hpp"

PyObject* encode_lz78(PyObject* self, PyObject* args);
PyObject* decode_lz78(PyObject* self, PyObject* args);

PyObject* encode_lz78_string(PyObject* self, PyObject* args);
PyObject* decode_lz78_string(PyObject* self, PyObject* args);

PyObject* encode_lzw(PyObject* self, PyObject* args);
PyObject* decode_lzw(PyObject* self, PyObject* args);

PyObject* encode_lzw_string(PyObject* self, PyObject* args);
PyObject* decode_lzw_string(PyObject* self, PyObject* args);

PyObject* encode_lz77(PyObject* self, PyObject* args);
PyObject* decode_lz77(PyObject* self, PyObject* args);

PyObject* encode_lz77_string(PyObject* self, PyObject* args);
PyObject* decode_lz77_string(PyObject* self, PyObject* args);

PyMethodDef methods[] = {
    {"encode_lz78", encode_lz78, METH_VARARGS, 0},
    {"decode_lz78", decode_lz78, METH_VARARGS, 0},
    {"encode_lz78_string", encode_lz78_string, METH_VARARGS, 0},
    {"decode_lz78_string", decode_lz78_string, METH_VARARGS, 0},
    {"encode_lzw", encode_lzw, METH_VARARGS, 0},
    {"decode_lzw", decode_lzw, METH_VARARGS, 0},
    {"encode_lzw_string", encode_lzw_string, METH_VARARGS, 0},
    {"decode_lzw_string", decode_lzw_string, METH_VARARGS, 0},
    {"encode_lz77", encode_lz77, METH_VARARGS, 0},
    {"decode_lz77", decode_lz77, METH_VARARGS, 0},
    {"encode_lz77_string", encode_lz77_string, METH_VARARGS, 0},
    {"decode_lz77_string", decode_lz77_string, METH_VARARGS, 0},
    {0},
};

#if PY_MAJOR_VERSION >= 3
PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "lz_encoders",
    0,
    -1,
    methods,
    0,
    0,
    0,
    0
};

PyObject * Initialize(PyObject * module) {
    return module;
}

extern "C" PyObject * PyInit_lz_encoders() {
	PyObject * module = PyModule_Create(&moduledef);
	return Initialize(module);
}

#else

extern "C" PyObject * initlz_encoders() {
	PyObject * module = Py_InitModule("lz_encoders", methods);
	return module;
}

#endif
