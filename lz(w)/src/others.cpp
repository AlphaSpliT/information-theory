#include "common.hpp"

PyObject *encode_lz78(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 2)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (input_f_name, output_f_name)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be string");
        return nullptr;
    }

    const char *input_file = PyUnicode_AsUTF8(arg1);
    const char *output_file = PyUnicode_AsUTF8(arg2);

    LZ78::encode(input_file, output_file);

    Py_RETURN_NONE;
}

PyObject *decode_lz78(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 2)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (input_f_name, output_f_name)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be string");
        return nullptr;
    }

    const char *input_file = PyUnicode_AsUTF8(arg1);
    const char *output_file = PyUnicode_AsUTF8(arg2);

    LZ78::decode(input_file, output_file);

    Py_RETURN_NONE;
}

PyObject *encode_lz78_string(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 1)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments.");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    unsigned char *data = (unsigned char *)PyBytes_AsString(PyUnicode_AsASCIIString(arg1));

    std::vector<LZ78::SaveStructure> save_data = LZ78::encode_data(data, PyUnicode_GET_SIZE(arg1));

    PyObject *list = PyList_New(save_data.size());

    for (size_t i = 0; i < save_data.size(); ++i)
    {
        auto saved = save_data[i];
        PyObject *dict = PyDict_New();

        PyDict_SetItem(dict, PyUnicode_FromString("index"), PyLong_FromLong(saved.index));
        PyDict_SetItem(dict, PyUnicode_FromString("character"), PyLong_FromLong(saved.character));

        PyList_SetItem(list, i, dict);
    }

    return list;
}
PyObject *decode_lz78_string(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 1)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments.");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);

    if (Py_TYPE(arg1) != &PyList_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    size_t list_size = PyList_GET_SIZE(arg1);
    std::vector<LZ78::SaveStructure> vec(list_size);

    for (size_t i = 0; i < list_size; ++i)
    {
        PyObject *current_item = PyList_GET_ITEM(arg1, i);

        if (Py_TYPE(current_item) != &PyDict_Type)
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict");
            return nullptr;
        }

        if (!PyDict_Contains(current_item, PyUnicode_FromString("character")))
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict containing character");
            return nullptr;
        }

        if (!PyDict_Contains(current_item, PyUnicode_FromString("index")))
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict containing index");
            return nullptr;
        }

        short character = PyLong_AsLong(PyDict_GetItemString(current_item, "character"));
        size_t index = PyLong_AsSsize_t(PyDict_GetItemString(current_item, "index"));

        vec[i] = LZ78::SaveStructure(index, character);
    }
    DataChunk result = LZ78::decode_data(vec);

    PyObject *result_string = PyUnicode_FromStringAndSize((const char *)result.get_buffer(), result.size());

    return result_string;
};

PyObject *encode_lzw(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 2)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (input_f_name, output_f_name)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be string");
        return nullptr;
    }

    const char *input_file = PyUnicode_AsUTF8(arg1);
    const char *output_file = PyUnicode_AsUTF8(arg2);

    LZW::encode(input_file, output_file);

    Py_RETURN_NONE;
}

PyObject *decode_lzw(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 2)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (input_f_name, output_f_name)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be string");
        return nullptr;
    }

    const char *input_file = PyUnicode_AsUTF8(arg1);
    const char *output_file = PyUnicode_AsUTF8(arg2);

    LZW::decode(input_file, output_file);

    Py_RETURN_NONE;
}

PyObject *encode_lz77(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 4)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (input_f_name, output_f_name, h_k, h_e)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);
    PyObject *arg3 = PyTuple_GetItem(args, 2);
    PyObject *arg4 = PyTuple_GetItem(args, 3);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg3) != &PyLong_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#3, must be long");
        return nullptr;
    }

    if (Py_TYPE(arg4) != &PyLong_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#4, must be long");
        return nullptr;
    }

    const char *input_file = PyUnicode_AsUTF8(arg1);
    const char *output_file = PyUnicode_AsUTF8(arg2);
    size_t h_k = PyLong_AsSsize_t(arg3);
    size_t h_e = PyLong_AsSsize_t(arg4);

    LZ77::encode(input_file, output_file, h_k, h_e);

    Py_RETURN_NONE;
}

PyObject *decode_lz77(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 2)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (input_f_name, output_f_name)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be string");
        return nullptr;
    }

    const char *input_file = PyUnicode_AsUTF8(arg1);
    const char *output_file = PyUnicode_AsUTF8(arg2);

    LZ77::decode(input_file, output_file);

    Py_RETURN_NONE;
}

PyObject *encode_lzw_string(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 1)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments.");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    unsigned char *data = (unsigned char *)PyBytes_AsString(PyUnicode_AsASCIIString(arg1));

    std::vector<LZW::SaveStructure> save_data = LZW::encode_data(data, PyUnicode_GET_SIZE(arg1));

    PyObject *list = PyList_New(save_data.size());

    for (size_t i = 0; i < save_data.size(); ++i)
    {
        auto saved = save_data[i];
        PyObject *dict = PyDict_New();
        PyDict_SetItem(dict, PyUnicode_FromString("index"), PyLong_FromLong(saved.index));
        PyList_SetItem(list, i, dict);
    }

    return list;
}
PyObject *decode_lzw_string(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 1)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments.");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);

    if (Py_TYPE(arg1) != &PyList_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    size_t list_size = PyList_GET_SIZE(arg1);
    std::vector<LZW::SaveStructure> vec(list_size);

    for (size_t i = 0; i < list_size; ++i)
    {
        PyObject *current_item = PyList_GET_ITEM(arg1, i);

        if (Py_TYPE(current_item) != &PyDict_Type)
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict");
            return nullptr;
        }

        if (!PyDict_Contains(current_item, PyUnicode_FromString("index")))
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict containing index");
            return nullptr;
        }
        size_t index = PyLong_AsSsize_t(PyDict_GetItemString(current_item, "index"));

        vec[i] = LZW::SaveStructure(index);
    }
    DataChunk result = LZW::decode_data(vec);

    PyObject *result_string = PyUnicode_FromStringAndSize((const char *)result.get_buffer(), result.size());

    return result_string;
};

PyObject *encode_lz77_string(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 3)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (string, h_k, h_e)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);
    PyObject *arg3 = PyTuple_GetItem(args, 2);

    if (Py_TYPE(arg1) != &PyUnicode_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be string");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyLong_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be long");
        return nullptr;
    }

    if (Py_TYPE(arg3) != &PyLong_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#3, must be long");
        return nullptr;
    }

    unsigned char *data = (unsigned char *)PyBytes_AsString(PyUnicode_AsASCIIString(arg1));
    size_t h_k = PyLong_AsSsize_t(arg2);
    size_t h_e = PyLong_AsSsize_t(arg3);

    std::vector<LZ77::SaveStructure> save_data = LZ77::encode_data(data, PyUnicode_GET_SIZE(arg1), h_k, h_e);

    PyObject *list = PyList_New(save_data.size());

    for (size_t i = 0; i < save_data.size(); ++i)
    {
        auto saved = save_data[i];
        PyObject *dict = PyDict_New();
        PyDict_SetItemString(dict, "index", PyLong_FromLong(saved.start_index));
        PyDict_SetItemString(dict, "length", PyLong_FromLong(saved.length));
        PyDict_SetItemString(dict, "character", PyLong_FromLong(saved.character));
        PyList_SetItem(list, i, dict);
    }

    return list;
}
PyObject *decode_lz77_string(PyObject *self, PyObject *args)
{
    size_t number_of_arguments = (size_t)PyTuple_GET_SIZE(args);

    if (number_of_arguments != 3)
    {
        PyErr_Format(PyExc_Exception, "Invalid number of arguments. (array, h_k, h_e)");
        return nullptr;
    }

    PyObject *arg1 = PyTuple_GetItem(args, 0);
    PyObject *arg2 = PyTuple_GetItem(args, 1);
    PyObject *arg3 = PyTuple_GetItem(args, 2);

    if (Py_TYPE(arg1) != &PyList_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#1, must be array");
        return nullptr;
    }

    if (Py_TYPE(arg2) != &PyLong_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#2, must be long");
        return nullptr;
    }

    if (Py_TYPE(arg3) != &PyLong_Type)
    {
        PyErr_Format(PyExc_Exception, "Invalid type for arg#3, must be long");
        return nullptr;
    }

    size_t list_size = PyList_GET_SIZE(arg1);
    std::vector<LZ77::SaveStructure> vec;
    size_t h_k = PyLong_AsSsize_t(arg2);
    size_t h_e = PyLong_AsSsize_t(arg3);

    for (size_t i = 0; i < list_size; ++i)
    {
        PyObject *current_item = PyList_GET_ITEM(arg1, i);

        if (Py_TYPE(current_item) != &PyDict_Type)
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict");
            return nullptr;
        }

        if (!PyDict_Contains(current_item, PyUnicode_FromString("index")))
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict containing index");
            return nullptr;
        }

        if (!PyDict_Contains(current_item, PyUnicode_FromString("index")))
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict containing character");
            return nullptr;
        }

        if (!PyDict_Contains(current_item, PyUnicode_FromString("index")))
        {
            PyErr_Format(PyExc_Exception, "Invalid list entry, must be dict containing length");
            return nullptr;
        }

        size_t index = PyLong_AsSsize_t(PyDict_GetItemString(current_item, "index"));
        size_t length = PyLong_AsSsize_t(PyDict_GetItemString(current_item, "length"));
        short character = PyLong_AsSsize_t(PyDict_GetItemString(current_item, "character"));

        vec.push_back(LZ77::SaveStructure(index, length, character));
    }

    LZ77::ActualSaveStructure save_structure;
    save_structure.h_k = h_k;
    save_structure.h_e = h_e;
    save_structure.number_of_structures = vec.size();
    save_structure.raw_data = (unsigned char *)&vec[0];

    DataChunk result = LZ77::decode_data(save_structure);

    PyObject *result_string = PyUnicode_FromStringAndSize((const char *)result.get_buffer(), result.size());

    return result_string;
};
